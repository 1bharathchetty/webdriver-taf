package com.binaryzombies.taf.webdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.testng.Assert;


// this class will have the methods for webdriver with complete
// error handling so the users need not worry writing for every web step
public class Websteps {
	
	private WebDriver browser;

	public Websteps (WebDriver browser){
		this.browser = browser;
	}
	
	//navigating to url
	public void gotoURL(String url){
		browser.get(url);
	}
	

	public void assertTitle(String expectedTitle){
		String actualTitle=browser.getTitle().trim();
		
		Assert.assertEquals(actualTitle, expectedTitle);
		
	}

	public void enter(By by, String testdata) {
		
		WebElement targetElement=getElement(by);
		
		targetElement.sendKeys(testdata);

	}
	public void click(By by) {
		WebElement targetElement=getElement(by);
		try{
			targetElement.click();
			
		}catch(StaleElementReferenceException e){
			//todos all exceptions
			throw new StaleElementReferenceException("");
		}catch(UnreachableBrowserException e){
			
		}catch(WebDriverException e){
			
		}
	}
	public void verifyElementPresent(By by) {
		Assert.assertTrue(isElementPresent(by));
	}
	public void verifyElementAbsent(By by) {
		Assert.assertFalse(isElementPresent(by));
	}

	
	public WebElement getElement(By by) {
		
		WebElement targetElement = null;
		try {
			targetElement = browser.findElement(by);
			if(targetElement.isDisplayed()){
				return targetElement;
			}else{
				
			}
		}catch(InvalidSelectorException e){
			throw new InvalidSelectorException("invalid selector");
		}catch(NoSuchElementException eObj){
			
			throw new NoSuchElementException("element is not finding or not visible");
			
		}catch(UnreachableBrowserException e){
			System.out.println();
		}catch(WebDriverException e){
			System.out.println();
		}
		return targetElement;
	}
	public boolean isElementPresent(By by) {
	
		
		WebElement targetElement;
		try {
			targetElement = browser.findElement(by);
			if(targetElement.isDisplayed()){
				return true;
			}else{
				System.out.println();
			}
		}catch(InvalidSelectorException e){
		}catch(NoSuchElementException e){
		}catch(UnreachableBrowserException e){
		}catch(WebDriverException e){
		}
		return false;
		
	}
}

