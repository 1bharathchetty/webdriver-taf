package com.binaryzombies.taf.webdriver;

import org.openqa.selenium.By;


/*
 * This class is written to implement custom locating strategies extending webdriver By. 
 * You could use driver.findElement(CustomBy.elementText('elementText')); 
 * Few more examples are available in CustomLocatorStrategyTests class in test folder
 */
public abstract class CustomBy extends By {
	
  /**
   * @param elementText The elementText to use
   * @return a By which locates elements via elementText
   */
  public static By elementText(final String elementText) {
    if (elementText == null)
      throw new IllegalArgumentException(
          "Cannot find elements when the elementText is null.");
    
    String xpathExpression = ".//*[text()='"+ elementText + "']";
    return new ByXPath(xpathExpression);
  }
  
  /**
   * @param partialElementText The partial elementText to use
   * @return a By which locates elements via partial elementText
   */
  public static By partialElementText(final String partialElementText) {
    if (partialElementText == null)
      throw new IllegalArgumentException(
          "Cannot find elements when the partial elementText is null.");
    
    String xpathExpression = ".//*[contains(text(),'"+ partialElementText + "')]";
    return new ByXPath(xpathExpression);
  }

  
  /**
   * @param partialID The  partialID to use
   * @return a By which locates elements via partialID 
   */
  public static By partialID(final String partialID) {
    if (partialID == null)
      throw new IllegalArgumentException(
          "Cannot find elements when the partialID is null.");
    
    String xpathExpression = ".//*[contains(@id,'"+ partialID + "')]";
    return new ByXPath(xpathExpression);
  }
  
  
  
  /**
   * @param partialName The  partialName to use
   * @return a By which locates elements via partialName 
   */
  public static By partialName(final String partialName) {
    if (partialName == null)
      throw new IllegalArgumentException(
          "Cannot find elements when the partialName is null.");
    
    String xpathExpression = ".//*[contains(@name,'"+ partialName + "')]";
    return new ByXPath(xpathExpression);
  }
}
