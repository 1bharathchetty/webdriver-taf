package testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import com.binaryzombies.taf.webdriver.CustomBy;

public class CustomLocatorStrategyTests {
	
	/*
	 * These tests are for the new locating strategy implemented in CustomBy class. 
	 */
  @Test
  public void f() {
	  
	  WebDriver driver = new ChromeDriver();
	  
	  WebElement elementById = driver.findElement(CustomBy.id("id"));
	  
	  WebElement elementByPartialId = driver.findElement(CustomBy.partialID("partialID"));
	  
	  WebElement elementByText = driver.findElement(CustomBy.elementText("elementText"));
	  
	  WebElement elementByPartialName = driver.findElement(CustomBy.partialName("partialName"));

	  WebElement elementByPartialText = driver.findElement(CustomBy.partialElementText("partialElementText"));

	  
  }
}
